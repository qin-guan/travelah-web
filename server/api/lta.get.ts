import {defineEventHandler, useQuery} from "h3";

import Fuse from 'fuse.js'

export default defineEventHandler(async (event) => {
  let {name} = useQuery(event)
  if (Array.isArray(name))
    return event.res.statusCode = 400

  const data = await $fetch('/api/bus-stops')

  const fuse = new Fuse(data, {
    keys: ['BusStopCode', 'RoadName', 'Description'],
  })

  return fuse.search(name)
})
