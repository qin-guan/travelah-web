import {defineEventHandler} from "h3";

interface LTABusStop {
  BusStopCode: string
  RoadName: string
  Description: string
  Latitude: number
  Longitude: number
}

const store: {
  data?: LTABusStop[]
} = {
  data: undefined
}

export default defineEventHandler(async () => {
  if (store.data)
    return store.data

  store.data = (await $fetch<{ value: LTABusStop[] }>('http://datamall2.mytransport.sg/ltaodataservice/BusStops', {
    headers: {
      'AccountKey': 'RdoZ93saQ32Ts1JcHbFegg=='
    }
  })).value

  return store.data
})
